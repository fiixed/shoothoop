﻿using UnityEngine;
using System.Collections;

public class ScoreKeeper : MonoBehaviour {

    private int score;

	// Use this for initialization
	void Start () {
        score = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void IncrementScore(int bonus) {

        score+=bonus;
        Debug.Log(score);
    }
}
